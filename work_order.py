# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import datetime
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool

__all__ = ['WorkOrder', 'WorkOrderReport']

STATES = {
    'readonly': Eval('state') != 'draft',
}

STATES_OPEN = {
    'readonly': Eval('state') != 'realization',
}

STATES_FINISHED = {
    'readonly': Eval('state') == 'finished',
}



class WorkOrder(Workflow, ModelSQL, ModelView):
    'Work Order'
    __name__ = 'audiovisual.work_order'
    _rec_name = 'number'
    number = fields.Char('Number', readonly=True)
    party = fields.Many2One('party.party', 'Party', required=True,
            states=STATES, select=True)
    kind = fields.Many2One('audiovisual.kind', 'Kind', states=STATES)
    description = fields.Text('Description', required=True,
            states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[ ('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}), 
            'company', 0)), ])
    realization_date = fields.Date('Realization Date', required=True,
            states=STATES_FINISHED)
    delivery_date = fields.Date('Delivery Date', states=STATES_FINISHED)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('realization', 'Realization'),
            ('inactive', 'Inactive'),
            ('approved', 'Approved'),
            ('finished', 'Finished'),
            ('canceled', 'Canceled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    notes = fields.Text('Notes', states=STATES_FINISHED)
    approval_date = fields.Date('Approval Date', states={
            'readonly': ~(Eval('state') == 'waiting'),
            })
    start_broadcast = fields.Date('Start Broadcast',
            states={
                'readonly': Eval('state') == 'finished',
                'required': Eval('state') == 'approved',
                'invisible': ~Eval('state').in_(['approved', 'finished']),  
            })
    end_broadcast = fields.Date('End Broadcast',
            states={
                'readonly': Eval('state') == 'finished',
                'required': Eval('state') == 'approved',
                'invisible': ~Eval('state').in_(['approved', 'finished']),
            })
    responsible = fields.Many2One('company.employee',
            'Responsible', states=STATES, required=True)
    life_time = fields.Function(fields.Integer('Life Time'),
            'get_life_time')
    sale = fields.Many2One('sale.sale', 'Sale', readonly=True,
            states=STATES, select=True)
    product = fields.Many2One('product.product', 'Product', required=True,
            domain=[('salable', '=', 'True')], states=STATES_FINISHED)
    cameraman = fields.Many2One('company.employee', 'Cameraman',
            states=STATES, select=True)
    editor = fields.Many2One('company.employee', 'Editor',
            states=STATES, select=True)


    @classmethod
    def __setup__(cls):
        super(WorkOrder, cls).__setup__()
        cls._order.insert(0, ('create_date', 'DESC'))
        cls._order.insert(1, ('id', 'DESC'))
        cls._error_messages.update({
            'missing_sequence_work_order': ('The sequence for '
                'work order is missing!'),
        })
        cls._transitions |= set((
                ('draft', 'realization'),
                ('draft', 'canceled'),
                ('realization', 'draft'),
                ('realization', 'canceled'),
                ('realization', 'inactive'),
                ('realization', 'approved'),
                ('inactive', 'realization'),
                ('inactive', 'canceled'),
                ('canceled', 'draft'),
                ('approved', 'finished'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': ~Eval('state').in_(
                        ['realization', 'canceled']
                        )
                    },
                'cancel': {
                    'invisible': Eval('state').in_(
                        ['approval', 'finished', 'canceled']
                        ),
                    },
                'realization': {
                    'invisible': ~Eval('state').in_(['draft', 'inactive']),
                    },
                'approved': {
                    'invisible': Eval('state') != 'realization',
                    },
                'inactive': {
                    'invisible': Eval('state') != 'realization',
                    },
                'finish': {
                    'invisible': Eval('state') != 'approved',
                    },
                })

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_realization_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, services):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('realization')
    def realization(cls, services):
        for service in services:
            service.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('inactive')
    def inactive(cls, services):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, services):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    def approved(cls, services):
        Date = Pool().get('ir.date')
        cls.write(services, {'approval_date': Date.today()})

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, services):
        pass

    def get_life_time(self, name=None):
        res = None
        if self.state != 'finished':
            res = datetime.today() - self.create_date
            res = res.days
        return res

    def set_number(self):
        '''
        Set sequence number
        '''
        if self.number:
            return
        pool = Pool()
        config = pool.get('audiovisual.configuration')(1)
        Sequence = pool.get('ir.sequence')
        if not config.audiovisual_work_order_sequence:
            self.raise_user_error('missing_sequence_work_order')

        seq_id = config.audiovisual_work_order_sequence.id
        self.write([self], {'number': Sequence.get_id(seq_id)})


class WorkOrderReport(Report):
    __name__ = 'audiovisual.work_order_report'
