# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.pyson import Eval, If

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Audiovisual Configuration'
    __name__ = 'audiovisual.configuration'
    audiovisual_work_order_sequence = fields.Many2One('ir.sequence',
        'Audiovisual Sequence', required=True, domain=[
                ('code', '=', 'audiovisual.work_order')])
    audiovisual_programmer_sequence = fields.Many2One('ir.sequence',
        'Audiovisual Programmer', required=True, domain=[
                ('code', '=', 'audiovisual.programmer')])
    efficay_hour_limit = fields.Integer('Efficay Hour Limit',
        required=True)
    min_time_kind = fields.Integer('Min Time in Kind',
        required=True, help='in seconds')
    max_time_kind = fields.Integer('Max Time in kind',
        required=True, help='in seconds')
    # company = fields.Many2One('company.company', 'Company', required=True,
    #     domain=[
    #         ('id', If(Eval('context', {}).contains('company'), '=', '!='),
    #             Eval('context', {}).get('company', 0)),
    #         ])
