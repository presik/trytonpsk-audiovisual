# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


__all__ = ['Kind']


class Kind(ModelSQL, ModelView):
    "Work Order Kind"
    __name__ = "audiovisual.kind"
    name = fields.Char('Name', required=True)
    limit_time = fields.Integer('Limit Time', required=True, help='In seconds')

    @classmethod
    def __setup__(cls):
        super(Kind, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))
        cls._error_messages.update({
            'invalid_time': ('Invalid time, the limit time must be between %s'),
        })


    @classmethod
    def validate(cls, kinds):
        for kind in kinds:
            kind.check_limit_time()

    def check_limit_time(self):
        pool = Pool()
        Config = pool.get('audiovisual.configuration')
        configs = Config.search([])
        config = configs[len(configs)-1]
        if config:
            if config.min_time_kind >= self.limit_time or config.max_time_kind <= self.limit_time:
                self.raise_user_error('invalid_time', str(config.min_time_kind) +' and '+ str(config.max_time_kind) + ' seconds')
