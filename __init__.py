# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
import kind
import work_order
import configuration
import programmer

def register():
    Pool.register(
        configuration.Configuration,
        programmer.Reel,
        programmer.Broadcast,
        programmer.Advertising,
        programmer.Programmer,
        programmer.AdvertisingReel,
        kind.Kind,
        work_order.WorkOrder,
        programmer.CreateProgrammerStart,
        programmer.AdvertisingWithoutAssign,
        module='audiovisual', type_='model')
    Pool.register(
        programmer.CreateProgrammer,
        module='audiovisual', type_='wizard')
    Pool.register(
        work_order.WorkOrderReport,
        module='audiovisual', type_='report')
