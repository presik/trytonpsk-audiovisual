# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement

from datetime import datetime, date, timedelta
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.pyson import Eval, If, In, Get
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateReport, StateTransition

__all__ = ['ReelTemplate', 'Programmer', 'Advertising', 'Broadcast',
    'AdvertisingReel', 'CreateProgrammerStart', 'CreateProgrammer', 'AdvertisingWithoutAssign']

STATES = {
    'readonly': Eval('state') != 'draft',
}

STATES_IN_PROCESS = {
    'readonly': Eval('state') != 'in_process',
}

STATES_FINISHED = {
    'readonly': Eval('state') == 'finished',
}


class Reel(ModelSQL, ModelView):
    'Reel'
    __name__ = 'audiovisual.broadcast.reel'
    _rec_name = 'number'
    broadcast = fields.Many2One('audiovisual.broadcast', 'Broadcast',
        required=True)
    number = fields.Integer('Number', required=True)
    min_time = fields.Integer('Min. Time', required=True, help='In seconds')
    max_time = fields.Integer('Max. Time', required=True, help='In seconds')


class Broadcast(ModelSQL, ModelView):
    'Broadcast'
    __name__ = 'audiovisual.broadcast'
    name = fields.Char('Name', required=True)
    active = fields.Boolean('Active')
    reels = fields.One2Many('audiovisual.broadcast.reel', 'broadcast', 'Reels', required=True)
    notes = fields.Char('Notes')


class Advertising(Workflow, ModelSQL, ModelView):
    'Advertising'
    __name__ = 'audiovisual.advertising'
    # _rec_name = 'party.name'
    party = fields.Many2One('party.party', 'Party', required=True,
        states=STATES, select=True, domain=[
            ('active', '=', True,)
            ])
    start_date = fields.Date('Start Date', required=True, states=STATES_FINISHED)
    end_date = fields.Date('End Date', required=True, states=STATES_FINISHED)
    description = fields.Char('Description', states=STATES_FINISHED)
    contract = fields.Many2One('sale.contract', 'Sale Contract', states=STATES, domain=[
        ('party', '=', Eval('party')),
        ('state', '=', 'confirmed'),
        ])
    product = fields.Many2One('product.product', 'Plan', required=True,
        domain=[('salable', '=', 'True')], states=STATES_FINISHED)
    broadcast = fields.Many2One('audiovisual.broadcast', 'Broadcast',
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[ ('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0)), ])
    impact_by_broadcast = fields.Integer('Impact By Broadcast',
        required=True, states=STATES)
    impact_time = fields.Integer('Impact Time', states=STATES,
        required=True, help='In seconds')
    # impact_done_rate = fields.Function(fields.Float('Impact Done Rate'),
    # 'get_impact_done_rate')
    state = fields.Selection([
            ('draft', 'Draft'),
            ('in_process', 'In Process'),
            ('on_hold', 'On Hold'),
            ('finished', 'Finished'),
            ('canceled', 'Canceled'),
        ], 'State', required=True)
    kind = fields.Many2One('audiovisual.kind', 'Kind', states=STATES)
    days_expired_contract = fields.Function(fields.Char('Days To Expiration of Contract', readonly=True), 'get_days_expiration')
    position_reel = fields.Selection([
            ('start', 'Start'),
            ('end', 'End'),
            ('intermedium', 'Intermedium'),
        ], 'Position Reel', required=True, states=STATES)

    @classmethod
    def __setup__(cls):
        super(Advertising, cls).__setup__()
        cls._order.insert(0, ('create_date', 'DESC'))
        cls._order.insert(1, ('id', 'DESC'))
        cls._error_messages.update({
            'missing_sequence_work_order': ('The sequence for '
                'work order is missing!'),
            'invalid_time': ('Invalid time, the impact time must not be greater what limit time in kind!'),
        })
        cls._transitions |= set((
            ('draft', 'in_process'),
            ('in_process', 'draft'),
            ('draft', 'canceled'),
            ('on_hold', 'canceled'),
            ('on_hold', 'in_process'),
            ('in_process', 'on_hold'),
            ('in_process', 'finished'),
            ('canceled', 'in_process'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(
                    ['realization', 'canceled']
                    )
                },
            'cancel': {
                'invisible': Eval('state').in_(
                    ['approval', 'finished', 'canceled']
                    ),
                },
            'in_process': {
                'invisible': ~Eval('state').in_(['draft', 'inactive']),
                },
            'on_hold': {
                'invisible': Eval('state') != 'realization',
                },
            'finish': {
                'invisible': Eval('state') != 'approved',
                },
        })

    def get_rec_name(self, name):
        if self.party:
            return self.party.name



    @classmethod
    def validate(cls, advertisings):
        for advertising in advertisings:
            advertising.check_impact_time()

    def check_impact_time(self):
        pool = Pool()
        Kind = pool.get('audiovisual.kind')
        kinds = Kind.search([
            ('id', '=', self.kind.id)
        ])
        if kinds:
            if kinds[0].limit_time < self.impact_time:
                self.raise_user_error('invalid_time')

    @fields.depends('contract', 'product', 'start_date', 'end_date')
    def on_change_contract(self):
        pool = Pool()
        Contract = pool.get('sale.contract')
        if self.contract:
            contract = Contract.search([
                ('id', '=', self.contract.id),
            ])
            lines = contract[0].product_lines
            self.start_date = contract[0].start_date
            self.end_date = contract[0].end_date
            self.product = lines[0].product.id


    def get_days_expiration(self, name):
        today = date.today()
        if self.end_date and today < self.end_date:
            days_res = self.end_date - today
            d = timedelta(days=1)
            days_exp = days_res.days
            return str(days_exp)
        else:
            return 'expired'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_position_reel():
        return 'intermedium'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    # def get_impact_done_rate(self, name=None):
    #     return

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('realization')
    def realization(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, records):
        pass


class Programmer(Workflow, ModelSQL, ModelView):
    'Programmer'
    __name__ = 'audiovisual.programmer'
    sequence = fields.Char('Sequence', readonly=True)
    release_date = fields.Date('Release Date', states=STATES_FINISHED)
    broadcast = fields.Many2One('audiovisual.broadcast', 'Broadcast',
        required=True, states=STATES)
    reels = fields.One2Many('audiovisual.programmer.advertising_reel',
        'programmer', 'Advertising Reel', states=STATES)
    reels_without_assign = fields.One2Many('audiovisual.programmer.advertising_without_assign',
        'programmer', 'Advertising pending to programmer', states=STATES)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('realization', 'Realization'),
            ('finished', 'Finished'),
            ('canceled', 'Canceled'),
        ], 'State', readonly=True, required=True)
    state_string = state.translated('state')
    notes = fields.Text('Notes', states=STATES_FINISHED)
    total_time = fields.Function(fields.Integer('Total Time', readonly=True, help='This is the Total Time programmer'), 'get_total_time')

    @classmethod
    def __setup__(cls):
        super(Programmer, cls).__setup__()
        cls._order.insert(1, ('broadcast', 'DESC'))
        cls._error_messages.update({
            'missing_sequence_work_order': ('The sequence for '
                'work order is missing!'),
            'advertising_empty': ('Advertising Empty'),
        })
        cls._transitions |= set((
            ('draft', 'realization'),
            ('draft', 'canceled'),
            ('realization', 'draft'),
            ('realization', 'canceled'),
            ('realization', 'finished'),
            ('finished', 'realization'),
            ('canceled', 'draft'),
        ))
        cls._buttons.update({
            'draft': {
                'invisible': ~Eval('state').in_(
                    ['canceled', 'realization']
                    )
                },
            'cancel': {
                'invisible': ~Eval('state').in_(
                    ['realization', 'draft']
                    ),
                },
            'realization': {
                'invisible': ~Eval('state').in_(['finished', 'draft']),
                },
            'finish': {
                'invisible': ~Eval('state').in_(['realization']),
                },
        })

    @classmethod
    def set_number(cls, requests):
        '''
        Fill the number field with the programmer sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('audiovisual.configuration')
        configs = Config.search([])
        config = configs[len(configs)-1]

        for request in requests:
            if request.sequence:
                continue
            if not config.audiovisual_programmer_sequence:
                continue
            number = Sequence.get_id(config.audiovisual_programmer_sequence.id)
            cls.write([request], {'sequence': number})

    @staticmethod
    def default_realization_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_state():
        return 'draft'

    def get_total_time(self, name):
        time = 0
        pool = Pool()
        Advertising_reel = pool.get('audiovisual.programmer.advertising_reel')
        if self.reels:
            adv_reel = Advertising_reel.search([
                ('programmer', '=', self),
            ])
            for a in adv_reel:
                if a.advertising:
                    if a.advertising.impact_time:
                        time = time + a.advertising.impact_time
        return time



    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('realization')
    def realization(cls, records):
        cls.set_number(records)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('finished')
    def finish(cls, records):
        pass

    def create_advertising_reels(self, values_reel):
        AdvertisingReel = Pool().get('audiovisual.programmer.advertising_reel')
        AdvertisingReel.create(values_reel)


    def create_advertising_pending(self, advertising_without_assign):
        pool = Pool()
        AdvertisingWithoutAssign = pool.get('audiovisual.programmer.advertising_without_assign')
        for adv in advertising_without_assign:
            res = {
                    'programmer': self,
                    'advertising': adv.id,
            }
            AdvertisingWithoutAssign.create([res])

    # def get_time(self, name=None):
    #     res = None
    #     if self.state != 'finished':
    #         res = datetime.today() - self.create_date
    #         res = res.days
    #     return res

class AdvertisingWithoutAssign(ModelSQL, ModelView):
    'Advertising Without Assing'
    __name__ = 'audiovisual.programmer.advertising_without_assign'
    programmer = fields.Many2One('audiovisual.programmer', 'Programmer',
        required=True)
    # acknowledgement = fields.Boolean('Acknowledgement')
    advertising = fields.Many2One('audiovisual.advertising', 'Advertising',
        required=True, select=True)
    plan = fields.Function(fields.Many2One('product.product', 'Plan', readonly=True), 'get_plan')
    impact_time = fields.Function(fields.Integer('Impact Time', readonly=True, help='in seconds'), 'get_impact_time')
    kind = fields.Function(fields.Many2One('audiovisual.kind', 'Kind', readonly=True), 'get_kind')
    #  domain=[
    #     ('broadcast', '=', Eval('broadcast'))
    # ]
    @classmethod
    def __setup__(cls):
        super(AdvertisingWithoutAssign, cls).__setup__()
        cls._order = [
            ('advertising', 'ASC'),
        ]

    def get_plan(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].product.id

    def get_impact_time(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].impact_time

    def get_kind(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].kind.id


class AdvertisingReel(ModelSQL, ModelView):
    'Advertising Reel'
    __name__ = 'audiovisual.programmer.advertising_reel'
    programmer = fields.Many2One('audiovisual.programmer', 'Programmer',
        required=True)
    # acknowledgement = fields.Boolean('Acknowledgement')
    advertising = fields.Many2One('audiovisual.advertising', 'Advertising',
        required=True, select=True)
    #  domain=[
    #     ('broadcast', '=', Eval('broadcast'))
    # ]
    reel = fields.Many2One('audiovisual.broadcast.reel', 'Reel', required=True)
    # domain=[
    #     ('programmer.broadcast', '=', Eval('broadcast'))
    # ]
    plan = fields.Function(fields.Many2One('product.product', 'Plan', readonly=True), 'get_plan')
    impact_time = fields.Function(fields.Integer('Impact Time', readonly=True, help='in seconds'), 'get_impact_time')
    kind = fields.Function(fields.Many2One('audiovisual.kind', 'Kind', readonly=True), 'get_kind')
    position = fields.Integer('Position in Reel', readonly=True)

    @classmethod
    def __setup__(cls):
        super(AdvertisingReel, cls).__setup__()
        cls._order = [
            ('reel', 'ASC'),
            ('position', 'ASC'),
        ]

    def get_plan(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].product.id

    def get_impact_time(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].impact_time

    def get_kind(self, name):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        if self.advertising:
            adv = Advertising.search([
                ('id', '=', self.advertising.id),
            ])
            return adv[0].kind.id


class CreateProgrammerStart(ModelView):
    'Create Programmer Start'
    __name__ = 'audiovisual.create_programmer.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    broadcast = fields.Many2One('audiovisual.broadcast', 'Broadcast',
        required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateProgrammer(Wizard):
    'Create Programmer'
    __name__ = 'audiovisual.create_programmer'
    start = StateView('audiovisual.create_programmer.start',
        'audiovisual.create_programmer_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Advertising = pool.get('audiovisual.advertising')
        Programmer = pool.get('audiovisual.programmer')
        advertisings = Advertising.search([
            ('broadcast', '=', self.start.broadcast.id),
            ('end_date', '>=', date.today()),
            ('state', '=', 'in_process'),
        ])
        if not advertisings:
            Programmer.raise_user_error('advertising_empty')
        start_date = self.start.start_date
        while start_date <= self.start.end_date:
            programmer_to_create = []
            values = self.get_values(self.start.broadcast, start_date)
            programmer_to_create.append(values)
            if programmer_to_create:
                programmers = Programmer.create(programmer_to_create)
                num = 0
                advertising_list = []
                latest = []
                for adv in advertisings:
                    advertising_list.append(adv)
                    if num > 0:
                        if adv.position_reel == 'start':
                            advertising_list[0], advertising_list[num] = advertising_list[num], advertising_list[0]
                        elif adv.position_reel == 'end':
                            latest.append(adv)
                            del(advertising_list[num])
                    num = num + 1
                if latest:
                    for l in latest:
                        advertising_list.append(l)

            #assing reel for each advertising

                PartyCategory = Pool().get('party.party-party.category')
                values_reel = []
                reels = [reel for reel in programmers[0].broadcast.reels]
                num_reels = len(reels)
                category_ids = []
                advertising_without_assign = []
                time_reel = {}
                for reel in reels:
                    time_reel[reel.id]={
                        'total_time': 0,
                        'position': 0,
                    }
                index = 0
                for advertising in advertising_list:
                    party_category = PartyCategory.search([
                        ('party', '=', advertising.party.id),
                    ])
                    num = 0
                    for reel in reels:
                        advertising_without_assign.append(advertising)
                        time_reel[reel.id]['total_time'] += advertising.impact_time
                        time_reel[reel.id]['position'] += 1

                        if time_reel[reel.id]['total_time'] > reel.max_time:
                            continue

                        advertising_without_assign.pop()

                        if num_reels >= 2:
                            for cat in party_category:
                                if cat.category.id in category_ids:
                                    if num+1 == num_reels:
                                        time_reel[reel.id]['position'] -= 1
                                        time_reel[reels[num-1].id]['position'] += 1
                                        values_reel.append(self.get_reel_line(time_reel[reels[num-1].id]['position'], advertising, reels[num-1].id, programmers[0]))
                                    else:
                                        time_reel[reel.id]['position'] -= 1
                                        time_reel[reels[num+1].id]['position'] += 1
                                        values_reel.append(self.get_reel_line(time_reel[reels[num+1].id]['position'], advertising, reels[num+1].id, programmers[0]))
                                else:
                                    category_ids.append(cat.category.id)
                                    values_reel.append(self.get_reel_line(time_reel[reel.id]['position'], advertising, reel.id, programmers[0]))
                        else:
                            values_reel.append(self.get_reel_line(time_reel[reel.id]['position'], advertising, reel.id, programmers[0]))

                        if advertising.impact_by_broadcast == num+1:
                            break
                        num = num + 1
                    index = index + 1


            #  creating advertising reels
                programmers[0].create_advertising_reels(values_reel)
                programmers[0].create_advertising_pending(advertising_without_assign)
            start_date = start_date + timedelta(days=1)

        return 'end'

    def get_values(self, broadcast, release_date):
        pool = Pool()
        Programmer = pool.get('audiovisual.programmer')
        values = {
            'broadcast': broadcast.id,
            'release_date': release_date,
        }
        return values

    def get_reel_line(self, position, advertising, reel_id, programmer):
        res = {
                'programmer': programmer.id,
                'advertising': advertising.id,
                'reel': reel_id,
                'position': position,
        }
        return res
